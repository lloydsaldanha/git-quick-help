Basic GIT Commands
====================

## Initializing a new directory

Creating a new (empty) local repo 

	// create the folder, enter the folder in terminal
	mkdir my_project
	cd my_project

	// init the folder as a git repo
	git init
	
	// point the repo to the online repo (origin)
	git remote add origin path/to/your/repo-name.git

OR

Clone from an existing repo
	
	// create folder, enter folder
	// clone repo in current ( . ) folder
	git clone path/to/your/repo-name.git .



## Committing

	// add all changes to staging area
	git add <file>

	// remove file from staging
	git rm --cached <file>

	// commit changes
	git commit -m 'some comment here'


## Pushing changes

By using normal push (merge)

	git pull origin master
	git push origin master

By using Rebase

	git pull --rebase origin master

	// if conflicts found, resolve conflicts, then `git add`
	// continue rebase
	git rebase --continue 
	`OR` 
	git rebase --skip 	
	`OR` 
	git rebase --abort

	// push changes to origin, after resolving conflicts
	git push origin master
