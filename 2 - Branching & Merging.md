Branching & Merging
=====================

reference : [atlassian branching tutorials](https://www.atlassian.com/git/tutorial/git-branches)

## Branching

### Creating branches

	// Create a new branch
	git branch my-branch-name


### Checkingout branches

	// switch to branch
	git checkout my-branch-name
	`OR`
	// create branch if not exists and checkout (switch to) branch
	git checkout -b my-branch-name
	`OR`
	// base new branch off existing branch
	git checkout my-new-branch another-existing-branch


### Misc branching operations

	// list branches
	git branch

	// delete branch ( safely )
	git branch -d branch-name-here

	// delete branch ( force )
	git branch -D branch-name-here


## Merging

	// checkout to branch you want to merge to
	git checkout master

	// merge current branch with provided branch
	git merge branch-name


## Example ( Mock Scenario )

	// create a branch
	git branch feature-branch master

	// checkout to feature branch, 
	// directory will now point to branch instead of master
	git checkout -b feature-branch

	// making changes to branch
	git add .
	git commit -m 'loads of messages here'
	# repeat this n times

	// ready to merge branch now
	// Switch to master
	git checkout master

	// merge feature branch with master ( note: since master is checked out )
	git merge feature-branch

	// delete feature branch, as it is NOT needed after merge
	git branch -d feature-branch


